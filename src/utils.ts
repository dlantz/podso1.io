import { Episode } from "./_stores";
const marked = require("marked");

export function retrieveEpisodeNumberFromTitle(title: string): string {
  return title.match(/\d+/)[0];
}

export function mapGrayMatterToEpisode(episode): Episode {
  return {
    ...episode.default.data,
    description: episode.default.content.trim(),
    descriptionHtml: marked(episode.default.content),
  };
}
