import fs from "fs";
import { mapGrayMatterToEpisode } from "../../utils";

const episodes = [];

const fileList = fs
  .readdirSync("./src/content/heritage")
  .filter((name) => name.match(/\.md$/));

for (const fileName of fileList) {
  episodes.push(
    mapGrayMatterToEpisode(require(`../../content/heritage/${fileName}`))
  );
}

episodes.sort(
  (ep1, ep2) =>
    new Date(ep2.publicationDate).getTime() -
    new Date(ep1.publicationDate).getTime()
);

export function get(req, res) {
  res.writeHead(200, {
    "Content-Type": "application/json",
  });

  res.end(JSON.stringify(episodes));
}
