---
title: 'About'
---

# About Stories from the Center of the Universe

Stories from the Center of the Universe is a podcast about hearing people's stories and reflecting on their lives, past, present, and future. We have been fortunate to be joined by all types of folks, including veterans, activists, conservatives, liberals, doctors, recovering addicts, spy catchers, Navy SEALs, and more.

The name 'Stories from the Center of the Universe' comes from a nickname for Ashland, Virginia, where we record our in-person podcasts. Physically there is no 'Center of the Universe', and given that it is nowhere and everywhere at once, we feel free to claim the location.

The goals of the podcast are:

- Demonstrate that the everyday people around us are more than what they seem
- Record and persist these mini-autobiographies for the family of the guest and future oral historians

<br />  
<br />

# Who We Are

## Paul Gilman

#### Host

Paul is an Army veteran, corporate executive, husband, and father of three. His playfulness and spontaneity make every episode a great time, and he loves getting people to tell stories. When he's not podcasting, you can find him coaching his daughter's basketball team or explaining how American sports work to Daniel.
