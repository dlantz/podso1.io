---
title: 'Episode 154: Josh Merkel'
subtitle: ''
explicit: 'false'
slug: /episode/154
publicationDate: '2022-05-30'
imgUrl: >-
  https://deow9bq0xqvbj.cloudfront.net/ep-logo/pbblog6800666/Josh_Merkel_2k5um3.jpeg
audioUrl: 'https://mcdn.podbean.com/mf/web/62uj92/Josh_Merkel_-_5_14_22_350_PM86luo.mp3'
size: 96481825
duration: 4019
status: published
type: episode
guid: podso1.podbean.com/ebc103b5-049d-3bec-b17b-13f2aaabf956
author: Stories from the Center of the Universe
season: 0
episodeNumber: 0
episodeType: full
categories:
  - Society & Culture
  - Comedy
---
Josh Merkel is the head coach of the Men’s RMC Basketball team who are the current Division III National Champions. We connected with Josh through Pedro Arruza (Episode 99) and he joins us to talk about his journey and RMC basketball. Josh is from Walkersville, MD, is the oldest of nine kids and he grew up on a farm. He spoke about his mom and dad and learning about having a passion for life from his mom and hard work from his dad. Josh knew at about age 15 that he wanted to coach basketball. He played college basketball at D3 Salisbury and told Paul how many NCAA tournaments he made as a player and as a coach. Josh and Paul discussed where he coached, who he learned from and some of his fondest memories from high school through being a head coach today. Josh talked a lot about this year‘s RMC basketball team. He spoke about national player of the year, Buzz Anthony, and their relationship. He also told Paul how he has the best forward in D3 in Miles Mallory and about some of the other impact players that brought home RMC‘s first ever national championship. Josh talked about the Randolph Macon community, the town of Ashland and his family. He shared how awesome his wife Morgan is and how they have found a rhythm managing careers and their three young sons. He finished by telling a great story about his oldest son Mason and how Mason did a little coaching at the ODAC tournament one year.