---
title: 'Episode 161: Cherud Wilkerson'
subtitle: ''
explicit: 'false'
slug: /episode/161
publicationDate: '2022-07-18'
imgUrl: >-
  https://deow9bq0xqvbj.cloudfront.net/ep-logo/pbblog6800666/Cherud_Wilkerson_family_pstb9b.jpg
audioUrl: >-
  https://mcdn.podbean.com/mf/web/tdb838/Cherud_Wilkerson_-_7_7_22_748_PM74v96.mp3
size: 132899445
duration: 5537
status: published
type: episode
guid: podso1.podbean.com/7b51a089-02d9-3782-b7ad-e0ac9909f4fc
author: Stories from the Center of the Universe
season: 0
episodeNumber: 0
episodeType: full
categories:
  - Society & Culture
  - Comedy
---
Cherud Wilkerson comes to the podcast courtesy of knowing occasional co-host Kevin. Cherud is from Long Island and he grew up going to different schools. He played football and wrestled in high school and even played against a famous Bengal QB. Football was his main sport and it got him looks from various colleges and he ended up playing at Va State. Cherud was in US Navy for eleven years. He started as enlisted and left as an officer. Cherud then told Paul and Kevin about his Covid-19 story. At the very beginning of Covid-19, Cherud got sick and he went to the hospital, back home and then back to hospital. He was in the hospital for twenty-one days. He said his good physical shape, mental fortitude and faith in God is what got him through. Cherud beat Covid-19 and that earned him a nickname from the hospital staff. They discussed how his battle with Covid-19 has changed him and they finished by talking about his family.