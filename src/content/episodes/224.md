---
title: 'Episode 224: Kate Chenery Tweedy'
subtitle: ''
explicit: 'false'
slug: /episode/224
publicationDate: '2023-07-31'
imgUrl: >-
  https://deow9bq0xqvbj.cloudfront.net/ep-logo/pbblog6800666/Kate_C_Tweedy_rm6hwx.jpeg
audioUrl: >-
  https://mcdn.podbean.com/mf/web/5rz3g9/Kate_Chenery_Tweedy_-_edited_-_7_28_23_1125_AM6ovq7.mp3
size: 123162458
duration: 5131
status: published
type: episode
guid: podso1.podbean.com/6e92e76c-80b2-3398-bcab-2aaac22c4db2
author: Stories from the Center of the Universe
season: 0
episodeNumber: 0
episodeType: full
categories:
  - Society & Culture
  - Comedy
---
Kate Chenery Tweedy, cousin of Silent Rob, joins the podcast to discuss Secretariat for Virginia and her story. Kate’s mom was Penny Chenery who was the force behind all things Secretariat who many consider the greatest race horse of all time. Kate talked about growing up in Colorado, but visiting Virginia often. She talked about going to boarding school, college and she told Paul the story of The Meadow and her grandfather. She spoke about being at the Belmont as Secretariat won the Triple Crown in 1973 and the movie they made about Secretariat. They finished by discussing what Kate is up to currently and about her daughters, Elena and Alice.