---
title: 'Episode 255: Earl Kinney'
subtitle: ''
explicit: 'false'
slug: /episode/255
publicationDate: '2024-01-25'
imgUrl: >-
  https://deow9bq0xqvbj.cloudfront.net/ep-logo/pbblog6800666/Earl_Kinney_sw5jnq.jpeg
audioUrl: >-
  https://mcdn.podbean.com/mf/web/vt2yj5/Earl_Kinney_-_edited_-_1_15_24_1006_AM82j6q.mp3
size: 133360245
duration: 5556
status: published
type: episode
guid: podso1.podbean.com/9b7a6094-5b5f-3508-9ec6-e5d4fe7caae1
author: Stories from the Center of the Universe
season: 0
episodeNumber: 0
episodeType: full
categories:
  - Society & Culture
  - Comedy
---
Earl Kinney had a busy time moving around in his first twelve years. He was born in West Virginia, moved to Richmond when he was six then went to New York City at eight and then his family settled in Ashland when he was twelve. They talked about his parents and what he did besides sports when he was growing up … which depended somewhat on where he was living! In sports, he was very good at both basketball and football. He discussed the college recruiting process and the advice he received. Football won out in the end and he played football for four years at Howard. After college he talked to Paul about what he almost did, what he did on the weekends and then what he found was his calling which was to be a teacher and especially a coach. He has had a long career as both teacher and coach and is currently the Head Football Coach at Godwin HS in Henrico County. They finished by discussing his boxing podcast he had during the pandemic and he told Paul about his family.