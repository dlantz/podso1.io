---
title: 'Episode 150: Bobby Malone'
subtitle: ''
explicit: 'false'
slug: /episode/150
publicationDate: '2022-05-05'
imgUrl: >-
  https://deow9bq0xqvbj.cloudfront.net/ep-logo/pbblog6800666/Bobby_Malone_diavnz.jpeg
audioUrl: >-
  https://mcdn.podbean.com/mf/web/ghfa92/Bobby_Malone_edited_-_5_1_22_906_PM67znf.mp3
size: 110284509
duration: 4595
status: published
type: episode
guid: podso1.podbean.com/a969f847-9432-3ea6-921c-c60f0898ba27
author: Stories from the Center of the Universe
season: 0
episodeNumber: 0
episodeType: full
categories:
  - Society & Culture
  - Comedy
---
Paul takes the podcast mobile for Episode 150 and interviewed Bobby Malone at the Rivah. Bobby grew up in Chesterfield and rode motorbikes and played sports growing up. He said he wasn’t big on school, but it allowed him to play sports and he explained why football was his favorite. Bobby also told Paul about a crazy injury he suffered playing baseball. After high school, Bobby had a successful career at Reynolds Metals and then decided to leave and go into real estate. Bobby has been coming to the Rivah for almost thirty years.  He and Paul talked about the Rivah, why Bobby loves it so much and Bobby even told Paul a couple of examples of the interesting wildlife that he has seen over the years. They discussed crabbing, fishing and how Bobby gardens oysters … some of which Paul gets to eat! Bobby told Paul some of the secrets about crabbing and fishing on the Rappahannock and dropped a couple of life lessons during the episode as well.  Finally, they talked about Bobby’s wife, Carolyn, and their blended family which includes seven grandchildren.