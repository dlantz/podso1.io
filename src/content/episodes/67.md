---
title: 'Episode 67: Amanda Ripley'
subtitle: ''
explicit: 'false'
slug: /episode/67
publicationDate: '2021-02-04'
imgUrl: 'https://deow9bq0xqvbj.cloudfront.net/ep-logo/pbblog6800666/Amanda_Ripley.jpg'
audioUrl: 'https://mcdn.podbean.com/mf/web/9rna7w/AmandaRipley.mp3'
size: 108477883
duration: 3389
status: published
type: episode
guid: podso1.podbean.com/5fb7d72f-2a3b-36a7-aee7-edfc79e7874d
author: Stories from the Center of the Universe
season: 0
episodeNumber: 0
episodeType: full
categories:
  - Society & Culture
  - Comedy
---
Amanda Ripley is a journalist and author whose work has been featured in Time Magazine, the New York Times, the Washington Post, the Wall Street Journal, and others. She has written three books on psychological and cultural themes varying from how we respond to disaster to how we get addicted to conflict.

Amanda talked with us about growing up in central NJ, the public vs private school experience, what it's like to be a journalist, and some of her extensive work, including her recent article entitled "The Mystery of Trust."  In it she explores why the US Military is currently the only institution that seems to hold American's trust, and how other institutions (like our beloved Congress) can learn from them on this. 

Amanda Ripley: https://www.amandaripley.com/

The Mystery of Trust: https://www.cardus.ca/comment/article/the-mystery-of-trust/