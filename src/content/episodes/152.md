---
title: 'Episode 152: Matt Williams'
subtitle: ''
explicit: 'false'
slug: /episode/152
publicationDate: '2022-05-16'
imgUrl: >-
  https://deow9bq0xqvbj.cloudfront.net/ep-logo/pbblog6800666/Matt_Williams_q93zz6.jpeg
audioUrl: 'https://mcdn.podbean.com/mf/web/kzmyru/Matt_Williams_-_5_8_22_813_AM8wh2c.mp3'
size: 121946824
duration: 5080
status: published
type: episode
guid: podso1.podbean.com/127511a3-3d1f-327b-83fc-c95b0807080e
author: Stories from the Center of the Universe
season: 0
episodeNumber: 0
episodeType: full
categories:
  - Society & Culture
  - Comedy
---
Paul has Matt Williams on this episode due to a connection Matt has with Boomer Muth (Episode 77). Matt and Boomer are in a band together (Acoustic Underground) and Paul and Matt talked about music and Matt being a bass guitarist. Matt was born in Delaware and grew up in Wilmington and Cincinnati, Ohio. He played baseball growing up, was a baseball fan and he got to attend many Phillies and Reds games in his youth. He made his way to Virginia when he attended the College of William and Mary. After college, Matt got a job in advertising in Philadelphia. After a couple of years, he moved down to Richmond and took a job with The Martin Agency. He discussed his journey at Martin which ended up with him being the CEO for his last five years of his tenure. He told the story of two very famous and successful advertising campaigns for one of the largest insurance companies in the U.S. as well as the ups and downs of being the CEO of a company. Paul and Matt finished by talking about what he is doing now and about his wife Betsy and their two sons, Patrick and Ethan.