---
title: 'Episode 221: Erika Johnson'
subtitle: ''
explicit: 'false'
slug: /episode/221
publicationDate: '2023-07-17'
imgUrl: >-
  https://deow9bq0xqvbj.cloudfront.net/ep-logo/pbblog6800666/Erika_Johnson_q92269.jpeg
audioUrl: >-
  https://mcdn.podbean.com/mf/web/dbv3u4/Erika_Johnson_edited_-_7_9_23_243_PMa9ie8.mp3
size: 110508326
duration: 4604
status: published
type: episode
guid: podso1.podbean.com/6260cc33-c01c-3407-b32a-8f88e4c204ba
author: Stories from the Center of the Universe
season: 0
episodeNumber: 0
episodeType: full
categories:
  - Society & Culture
  - Comedy
---
Erika Johnson lived in the east end of Henrico and then her family moved to Hanover when she was in elementary school. Erika and Paul know each other from working together a while ago at a large Richmond area employer. They discussed her career path from start to now and her love for music and how it helps put you in the moment. They are both UVA grads and they talked about Erika’s experience at UVA especially her choice of major and her sorority.