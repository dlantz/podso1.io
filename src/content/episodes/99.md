---
title: 'Episode 99: Pedro Arruza'
subtitle: ''
explicit: 'false'
slug: /episode/99
publicationDate: '2021-08-09'
imgUrl: >-
  https://deow9bq0xqvbj.cloudfront.net/ep-logo/pbblog6800666/Pedro_Arruza_jpeg_mv3syt.jpg
audioUrl: 'https://mcdn.podbean.com/mf/web/882bti/Pedro_Arruza_-_8_7_21_926_AMabsxs.mp3'
size: 137533149
duration: 5730
status: published
type: episode
guid: podso1.podbean.com/ae288386-aadd-3d25-8b41-b5834b402cae
author: Stories from the Center of the Universe
season: 0
episodeNumber: 0
episodeType: full
categories:
  - Society & Culture
  - Comedy
---
Pedro Arruza is the Head Football Coach at Randolph-Macon College.  Pedro was born in Spain and lived in England and Mexico before settling in West Palm Beach, Florida.  He convinced his mom to let him play football in 7th grade and he was hooked. Paul asked Pedro why football became a career and Pedro stated it was the impact that his coaches had on him that paved his path to being an educator and a coach.

Coach Arruza was a stand out in high school and received many offers to play football at the college level.  After visiting Wheaton College, he knew it was the place for him.  He was a 4 year starter there and earned several All-American honors while playing at Wheaton. After graduating, Pedro was an assistant coach at the high school and college level and then was hired as the RMC Head Coach in 2004 at the very young age of 30.

Pedro discussed with Paul and Kevin his recruiting philosophy, his journey to faith, his passions and how much he loves Ashland.  He talked about how the culture and mindset of RMC Football has changed since he arrived and how much he loves his players.  Pedro talked about his family starting with how blessed he is to have married his wife Kara.  They have three children, Owen (19), Elle (17) and Will (14).