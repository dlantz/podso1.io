---
title: 'Episode 134: Growing Up in Ashland (1970s and 80s)'
subtitle: ''
explicit: 'false'
slug: /episode/134
publicationDate: '2022-01-24'
imgUrl: >-
  https://deow9bq0xqvbj.cloudfront.net/ep-logo/pbblog6800666/Duke_Rob_and_Paul_6nk7ss.jpg
audioUrl: >-
  https://mcdn.podbean.com/mf/web/yiegzt/Growing_up_in_Ashland_edited_-_Duke_Rob_and_Paul_-_1_15_22_945_AM770dc.mp3
size: 121697303
duration: 5070
status: published
type: episode
guid: podso1.podbean.com/be98a4bc-a7a0-3a6d-9535-418c6c5f928f
author: Stories from the Center of the Universe
season: 0
episodeNumber: 0
episodeType: full
categories:
  - Society & Culture
  - Comedy
---
Duke, Rob and Paul have known each other almost 50 years. On this unique episode, we discussed growing up in Ashland in the 1970s and 80s. We had fun reminiscing and recording stories from our childhood. We hope you will enjoy it as well.