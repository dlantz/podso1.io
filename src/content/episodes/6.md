---
title: 'Episode 6: Dimitry Droujinsky (pt. 2)'
subtitle: ''
explicit: 'false'
slug: /episode/6
publicationDate: '2020-03-01'
imgUrl: >-
  https://deow9bq0xqvbj.cloudfront.net/ep-logo/pbblog6800666/IMG_0030-preview.jpg
audioUrl: 'https://mcdn.podbean.com/mf/web/s9ys52/ep5_dimitry_pt2_edited.mp3'
size: 97635622
duration: 4790
status: published
type: episode
guid: podso1.podbean.com/da2576c3-e6d2-5d2a-b087-5bbaf8376e18
author: Stories from the Center of the Universe
season: 0
episodeNumber: 0
episodeType: full
categories:
  - Society & Culture
  - Comedy
---
Our conversation continues with FBI Spycatcher Dimitry Droujinsky as he recalls his memories of Kuwait, the Cuban Missile Crisis, the Berlin Wall, his upbringings in Palestine, and his experiences in fatherhood.