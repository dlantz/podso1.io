---
title: 'Episode 232: Rich Dalton - Part 2'
subtitle: ''
explicit: 'false'
slug: /episode/232
publicationDate: '2023-09-18'
imgUrl: >-
  https://deow9bq0xqvbj.cloudfront.net/ep-logo/pbblog6800666/Rich_Dalton_pt_2_yk4iaj.jpeg
audioUrl: >-
  https://mcdn.podbean.com/mf/web/nwqtqr/Rich_Dalton_Part_2_-_9_16_23_537_PM9rt95.mp3
size: 105290942
duration: 4386
status: published
type: episode
guid: podso1.podbean.com/8f688e07-9e17-3cc3-a130-d55bc25a4652
author: Stories from the Center of the Universe
season: 0
episodeNumber: 0
episodeType: full
categories:
  - Society & Culture
  - Comedy
---
Paul brings back Rich Dalton for Part 2 of a three part series. They completed a quick recap of Part 1 and then discussed life after the Marine Corps. Rich talked about his interest in law enforcement, but that he decided to go back to college and wrestle. He talked about living in California, but moving back to New Jersey for a brief time and then how he changed his career path and got certified as a personal trainer. Rich did work in law enforcement, but also told Paul about his MMA career including his fighting style and one particular event that was held at the Playboy Mansion. Stay tuned for Part 3 …