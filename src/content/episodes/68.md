---
title: 'Episode 68: Thoughts on Myanmar with Pete Silvester'
subtitle: ''
explicit: 'false'
slug: /episode/68
publicationDate: '2021-02-08'
imgUrl: 'https://deow9bq0xqvbj.cloudfront.net/ep-logo/pbblog6800666/pete_silvester.jpeg'
audioUrl: 'https://mcdn.podbean.com/mf/web/4fjhc5/PeteSilvester_7dwbn.mp3'
size: 152085238
duration: 4752
status: published
type: episode
guid: podso1.podbean.com/1396a120-91d2-3e56-8d72-b2a43d387036
author: Stories from the Center of the Universe
season: 0
episodeNumber: 0
episodeType: full
categories:
  - Society & Culture
  - Comedy
---
Pete Silvester is a social scientist, entrepreneur, and proud Territorian who has traveled the world by virtue of a career in International Aid and Development. He has spent many years in SE Asia, and came on to give us his take on the events in Myanmar. 

Myanmar, formerly Burma, was under military rule for 50 years before it began its experiment with democracy in the early 2010s. As it reopened to the outside world, the international community looked on with enthusiasm, and financial aid and investments began to flow into the country. Pete jumped right into the mix, moving to Yangon to establish a company that served as both a startup incubator and a consultancy to help build an inclusive private sector in Myanmar. He and his family now live in Thailand. 

In our conversation, Pete tells us about the Territorian upbringing, his travel, the wanderlust that is shared by many in the international aid scene, and his perspective on Myanmar, the Rohingya, Aung San Suu Kyi, and of course the military coup that happened one week ago on February first.