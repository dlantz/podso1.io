---
title: 'Episode 259: Rabbi Sherry Grinsteiner'
subtitle: ''
explicit: 'false'
slug: /episode/259
publicationDate: '2024-02-12'
imgUrl: >-
  https://deow9bq0xqvbj.cloudfront.net/ep-logo/pbblog6800666/Rabbi_Sherry_sn6isp.jpeg
audioUrl: >-
  https://mcdn.podbean.com/mf/web/434eu9/Rabbi_Sherry_-_edited_-_2_10_24_805_AM60zs4.mp3
size: 126797450
duration: 5283
status: published
type: episode
guid: podso1.podbean.com/d9dbe281-abbf-3d2a-b0cd-e4a4be02c1a4
author: Stories from the Center of the Universe
season: 0
episodeNumber: 0
episodeType: full
categories:
  - Society & Culture
  - Comedy
---
Rabbi Sherry Grinsteiner joins Paul via a connection through Ben Ipson (Episode 252). They first spoke about how Rabbi Sherry recently delivered the convocation at the Virginia House of Delegates. She told Paul about her parents being from Yemen and how she was born in Israel. She told Paul she had a wonderful childhood even though she lived through and amongst wars … “it was just life”. They talked about her service as an officer in the Israeli Army and the culture change when she moved to the United States. She told Paul how she met and married a tall handsome U.S. Marine and their children. She became a U.S. Citizen on 10-31-2000 and she told Paul how happy and proud she is to be an American. They discussed the events on October 7, 2023 and the sheer panic and terror she felt for the people, including her family, in Israel. They finished by talking about human nature and Rabbi Sherry gave a wonderful analogy about how we can and should all get along.