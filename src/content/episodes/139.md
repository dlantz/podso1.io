---
title: 'Episode 139: Jack Luck'
subtitle: ''
explicit: 'false'
slug: /episode/139
publicationDate: '2022-02-21'
imgUrl: >-
  https://deow9bq0xqvbj.cloudfront.net/ep-logo/pbblog6800666/Jack_Luck_z3agdn.png
audioUrl: 'https://mcdn.podbean.com/mf/web/9cbdn7/Jack_Luck_-_2_10_22_958_AM681zz.mp3'
size: 103043993
duration: 4293
status: published
type: episode
guid: podso1.podbean.com/d8f9d9f5-e812-3561-9e6e-30d9e32f3062
author: Stories from the Center of the Universe
season: 0
episodeNumber: 0
episodeType: full
categories:
  - Society & Culture
  - Comedy
---
Jack Luck is an Ashland icon and he joins Paul along with his oldest son, Ross. Jack talked about growing up on a farm in Ashland which is now part of the I-95 interchange. Jack told Paul about how he and his brother went to Randolph-Macon Academy and how he got a nickname that even Ross had never heard before. They discussed his time at Randolph-Macon College where he played football, was inducted in to the RMC Athletic Hall of Fame and his consistent support of the college. Ross talked about the family business, Luck Motor Company, as well as what it was like growing up with Jack as his dad. Jack also talked about his wife Shearer, their four kids and their seven grandchildren.