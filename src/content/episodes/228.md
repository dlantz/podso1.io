---
title: 'Episode 228: Bill Gatewood - DAA'
subtitle: ''
explicit: 'false'
slug: /episode/228
publicationDate: '2023-08-21'
imgUrl: >-
  https://deow9bq0xqvbj.cloudfront.net/ep-logo/pbblog6800666/Bill_Gatewood_Stories_from_the_Center_of_the_Universe_2023-09_kci2xx.jpg
audioUrl: >-
  https://mcdn.podbean.com/mf/web/nu95dj/Bill_Gatewood_-_DAA-_edited_-_8_19_23_830_AMbspty.mp3
size: 46549285
duration: 1939
status: published
type: episode
guid: podso1.podbean.com/0ecc3bc0-ad5f-34a1-ad8b-1be3e2c7f848
author: Stories from the Center of the Universe
season: 0
episodeNumber: 0
episodeType: full
categories:
  - Society & Culture
  - Comedy
---
Bill Gatewood joins Paul for a Downtown Ashland Association episode. Bill and Paul talked about how he got involved with the organization which led a discussion about Train Day in Ashland. They also discussed what brought Bill to Ashland originally and then how he served the town as a police officer for almost thirty-five years. Bill continues to volunteer in the Center of the Universe and told Paul how interested folks can volunteer with the Downtown Ashland Association.