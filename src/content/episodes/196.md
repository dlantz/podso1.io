---
title: 'Episode 196: George and James Monroe'
subtitle: ''
explicit: 'false'
slug: /episode/196
publicationDate: '2023-02-06'
imgUrl: >-
  https://deow9bq0xqvbj.cloudfront.net/ep-logo/pbblog6800666/George_monroe_wx82a5.jpeg
audioUrl: >-
  https://mcdn.podbean.com/mf/web/iymku8/James_to_George_Monroe_edited_-_2_4_23_540_PM82evx.mp3
size: 64893514
duration: 2703
status: published
type: episode
guid: podso1.podbean.com/20c9c704-3227-3e0a-a25d-fdba95771e60
author: Stories from the Center of the Universe
season: 0
episodeNumber: 0
episodeType: full
categories:
  - Society & Culture
  - Comedy
---
Paul and Kevin welcome back George Monroe to the podcast. George (Episode 170) joined the first time to tell his story and returns to tell the story of his family history. Paul started with a question of when George started to understand and question his family’s history. George said he was young, but that the subject was not discussed very often amongst the adults. Over time, George has become the griot for his family and he will probably pass that on to one of his kids who has the same curiosity and interest as his dad. George, Paul and Kevin discussed interviews, articles and speeches that George has been a part of as well as what he hopes to accomplish in the next six to twelve months. They also talked about some modern day topics and they wrapped up with Paul inviting George back and George thanking Paul and Kevin for the platform to tell his family’s story.