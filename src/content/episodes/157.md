---
title: 'Episode 157: Dave Tribble'
subtitle: ''
explicit: 'false'
slug: /episode/157
publicationDate: '2022-06-20'
imgUrl: >-
  https://deow9bq0xqvbj.cloudfront.net/ep-logo/pbblog6800666/Dave_Tribble_5ua5e4.jpg
audioUrl: >-
  https://mcdn.podbean.com/mf/web/2yzfmy/Dave_Tribble_Edited_Final_-_6_11_22_919_AMb9qmv.mp3
size: 115526971
duration: 4813
status: published
type: episode
guid: podso1.podbean.com/a2253662-2d47-3cbf-8cdb-1701261c45ad
author: Stories from the Center of the Universe
season: 0
episodeNumber: 0
episodeType: full
categories:
  - Society & Culture
  - Comedy
---
Dave Tribble knows Paul’s dad Larry (Ep. #9) and he joins Paul on the podcast. Dave is from the Richmond area and after his U.S. Army service was completed, he and his wife settled in Hanover, Virginia.  Dave talked about growing up, a long list of jobs he held as a young man and a car accident that changed his life. Dave is a United States Army Veteran and he and Paul talked extensively about his time in Vietnam. Dave came home in 1970 and had a hard time finding and keeping a job. His father-in-law got him an apprentice position in the electrical field and that led to a lifelong career with Dave eventually owning his own company. Paul and Dave talked about his likes and dislikes of running a company, why he likes central Virginia and he also told Paul about a “lawyer” joke that he told Larry a while back. They finished by talking about his family and his favorite hobby. Dave’s wife is Betty and they have three sons (Kevin, Dave Jr. and Cory) and he and Betty have nine grandchildren. He and Paul then talked about golf … how much he loves it, some good stories and the life lessons it teaches you. 

This episode was recorded not too long after Memorial Day. In memory and deep appreciation of Dave's friends lost too soon and for all those who made the ultimate sacrifice.