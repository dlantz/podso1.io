---
title: 'Episode 10: Richie Crabbe'
subtitle: ''
explicit: 'false'
slug: /episode/10
publicationDate: '2020-03-19'
imgUrl: 'https://deow9bq0xqvbj.cloudfront.net/ep-logo/pbblog6800666/IMG_0333.jpg'
audioUrl: 'https://mcdn.podbean.com/mf/web/i6pawe/20200311_RichieCrabbe_1.mp3'
size: 100032781
duration: 4592
status: published
type: episode
guid: podso1.podbean.com/49733f81-c35e-552f-9ce1-ee9aea2e8c62
author: Stories from the Center of the Universe
season: 0
episodeNumber: 0
episodeType: full
categories:
  - Society & Culture
  - Comedy
---
Richie Crabbe is a musician, barbecue enthusiast, former home-brewer, and father of two. This episode, Richie tells us about his upbringing in small-town Virginia, his band Stagg Creek (catch them at Hanover Country Club), and his adventures custom-building a sheet metal barbecue grill big enough to feed 1000 people.