---
title: 'Episode 245: Mark Jenkins Part 2'
subtitle: ''
explicit: 'false'
slug: /episode/245
publicationDate: '2023-11-20'
imgUrl: 'https://deow9bq0xqvbj.cloudfront.net/ep-logo/pbblog6800666/Israel_jfub8g.jpeg'
audioUrl: >-
  https://mcdn.podbean.com/mf/web/qyb7en/Mark_Jenkins_Part_2_-_edited_-_11_17_23_231_PM7ossw.mp3
size: 90632486
duration: 3776
status: published
type: episode
guid: podso1.podbean.com/e351e82b-1e12-39fb-a25f-eb8f8155c8af
author: Stories from the Center of the Universe
season: 0
episodeNumber: 0
episodeType: full
categories:
  - Society & Culture
  - Comedy
---
Mark Jenkins returns for his second episode to continue to talk about Israel. Paul brought up that this is the first time Israel has declared a state of war since 1973. Mark talked about how that is different from past “operations” as well how there is a war of public opinion. Mark went on to discuss some key historical events regarding Israel, Palestine and the Jewish people. Mark addressed past and current administrations and the response of the United States and what has changed over the last sixty years or so. They finished by talking about how this is an ever evolving story … which is why Mark will be back in the future!