---
title: 'Episode 166: Tony Tran'
subtitle: ''
explicit: 'false'
slug: /episode/166
publicationDate: '2022-08-15'
imgUrl: >-
  https://deow9bq0xqvbj.cloudfront.net/ep-logo/pbblog6800666/ttran_feb2020-130color_iedxaz.jpg
audioUrl: >-
  https://mcdn.podbean.com/mf/web/xfmdvh/Podcast_with_Tony_Tran_Edited_-_8_7_22_221_PMa2b03.mp3
size: 116745113
duration: 4864
status: published
type: episode
guid: podso1.podbean.com/6a8a67ab-fd00-316a-986e-8f0cf8e69973
author: Stories from the Center of the Universe
season: 0
episodeNumber: 0
episodeType: full
categories:
  - Society & Culture
  - Comedy
---
Tony Tran comes to the podcast through Mike DiMaggio who he went to school with and Paul knows Mike via a family connection. Tony and Paul talked about many things, but started with an amazing survival story for Tony and his family. Tony is from Vietnam and he and his family had to escape during the fall of Saigon. They discussed the memories he had before they left and the amazing journey they went on to get to the United States. Tony gave deep reverence to his parents and the way he and his siblings were raised. He told Paul about the culture change of coming to the USA and about growing up in the Chicago suburbs. Tony went to the University of Illinois and he and Paul talked about jobs he had and his career in software engineering. Tony dropped a couple of his keys to success during this part of their conversation. He told Paul what he has done and what he enjoys doing in his retirement which includes spending time in Barcelona, Spain. The way Tony talked about Barcelona, Paul declared that a visit there is now on his bucket list!