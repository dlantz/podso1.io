---
title: 'Heritage 2: John Wesley Jones'
subtitle: The Four Eldest Lantz Brothers
explicit: true
slug: /heritage/2
publicationDate: 2020-07-21
imgUrl: https://cdn.scotupodcast.com/images/JohnWesleyJones.jpg
audioUrl: https://cdn.scotupodcast.com/audio/heritage/JohnWesleyJones.mp3
# automate this
size: 139578227
duration: 4362
status: published
type: heritage-episode
guid: 1A11C092-E192-4A2F-8733-D1B77E4BDE09
author: Pod So 1
season: 0
episodeNumber: 0
episodeType: full
categories:
  - Society
  - Culture
---

Mr. John Wesley “Wes” Jones, Jr., age 98, of Union Level, VA was born on July 11, 1921 and passed away on May 27, 2020 in his home surrounded by his family. Mr. Jones was preceded in death by his beloved wife of 51 years, Bevelyn Glover Jones. He is survived by his two daughters: Theresa J. Shabenas of Bracey, VA. and Elaine J. Stepp and her husband Craig of Apex, NC; his two grandchildren: Christopher Jones and his wife Beth of South Hill, VA and Katherine Bagley and her husband Christopher of Lafayette, IN and his six great-grandchildren.

Mr. Jones served proudly in the 358th Regiment 90th Infantry Division serving under the command of General George S. Patton. Mr. Jones takes great pride in the fact that he shook General Patton’s hand. In the midst of the Battle of the Bulge, Mr. Jones was awarded the Bronze Star. After the war ended he was reassigned to the 26th Infantry Regiment, 1st Division to guard 13 top war criminals during the Nuremberg Trials. Mr. Jones was the eldest member of the South Hill American Legion Post 79 and a life long member of Zion United Methodist Church, Union Level, VA. He was an electrician by trade but his true passion was wood working.
