import { writable } from "svelte/store";

export interface Episode {
  title: string;
  slug: string;
  imgUrl: string;
  description: string;
  descriptionHtml: string;
  audioUrl: string;
}
export const currentEpisode$ = writable<Episode | undefined>(undefined);
