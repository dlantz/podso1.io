/**
 * this is a WIP
 */

const mp3Duration = require('mp3-duration');
const fs = require('fs');
const uuid = require('uuid');

const filePath = '/Users/daniellantz/Downloads/SaltyStories3.mp3';

mp3Duration(filePath, function (err, duration) {
  if (err) return console.log(err.message);
  const stats = fs.statSync(filePath);
  const fileSizeInBytes = stats.size;
  const md = fillTemplate(duration, fileSizeInBytes);
  console.log(md);
});

const fillTemplate = (duration, fileSize) => {
  return `
  ---
  title: #TODO
  subtitle: #TODO 
  explicit: #TODO
  slug: #TODO
  publicationDate: ${new Date().toISOString().split('T')[0]}
  imgUrl: #TODO
  audioUrl: #TODO
  size: ${fileSize}
  duration: ${Math.floor(duration)}
  status: published
  type: heritage-episode
  guid: ${uuid()}
  author: Stories from the Center of the Universe
  season: 0
  episodeNumber: 0
  episodeType: full
  categories:
    - Society
    - Culture
  ---
  `;
};
