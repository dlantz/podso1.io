var fetch = require("node-fetch");
var parser = require("xml2json");
var fs = require("fs");

fetch("https://podso1.podbean.com/feed.xml")
  .then((res) => res.text())
  .then((body) => {
    fs.writeFileSync("static/podbeanfeed.xml", body);
    const json = parser.toJson(body);
    output = json.replace(/http:/g, "https:");
    fs.writeFile("static/feed.json", output, (err) => {
      if (err) throw err;
      console.log("File has been saved");
    });
  });
