const yaml = require('js-yaml');
const fs = require('fs');
var TurndownService = require('turndown');

var turndownService = new TurndownService();

const data = fs.readFileSync('static/feed.json');
const feed = JSON.parse(data);
const episodes = feed.rss.channel.item;

const convertHoursToSeconds = (hours) => {
  const units = hours.split(':');
  return units.length === 3
    ? parseInt(units[0], 10) * 3600 +
        parseInt(units[1], 10) * 60 +
        parseInt(units[2], 10)
    : parseInt(units[0], 10) * 60 + parseInt(units[1], 10);
};
for (const episode of episodes) {
  const episodeNumber = episode.title.match(/\d+/)[0];

  const toDump = {
    title: episode.title,
    subtitle: '',
    explicit: episode['itunes:explicit'],
    slug: '/episode/' + episodeNumber,
    publicationDate: new Date(episode.pubDate).toISOString().substr(0, 10),
    imgUrl:
      episode['itunes:image']?.href ||
      'https://deow9bq0xqvbj.cloudfront.net/image-logo/6800666/scotu.png',
    audioUrl: episode.enclosure.url,
    size: parseInt(episode.enclosure.length, 10),
    duration: convertHoursToSeconds(episode['itunes:duration']),
    status: 'published',
    type: 'episode',
    guid: episode.guid.$t,
    author: 'Stories from the Center of the Universe',
    season: 0,
    episodeNumber: 0,
    episodeType: 'full',
    categories: ['Society & Culture', 'Comedy'],
  };
  let outputString = '---\n';
  outputString += yaml.safeDump(toDump);
  outputString += '---\n';
  outputString += turndownService.turndown(episode.description);

  fileName = episodeNumber + '.md';

  fs.writeFileSync('src/content/episodes/' + fileName, outputString);
}
