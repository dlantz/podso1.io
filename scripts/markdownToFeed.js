"use strict";

const RSS = require("rss");
const yaml = require("js-yaml");
const fs = require("fs");
const matter = require("gray-matter");
const { exit } = require("process");
const marked = require("marked");

const getFeedOptions = (podcastData) => {
  return {
    title: podcastData.title,
    description: podcastData.description,
    site_url: podcastData.siteUrl,
    feed_url: podcastData.feedUrl,
    image_url: podcastData.imageUrl,
    language: podcastData.language,
    copyright: podcastData.copyright,
    docs: `https://help.apple.com/itc/podcasts_connect/#/itcb54353390`,
    author: podcastData.authorName,
    managingEditor: podcastData.managingEditor,
    webMaster: podcastData.webMaster,
    categories: [
      podcastData.category1,
      podcastData.category2,
      podcastData.category3,
    ],
    pubDate: podcastData.publicationDate,
    ttl: podcastData.timeToLive,
    generator: `https://github.com/miller-productions/gatsby-plugin-podcast-feed-mdx`,
    custom_namespaces: {
      itunes: "http://www.itunes.com/dtds/podcast-1.0.dtd",
      googleplay: "http://www.google.com/schemas/play-podcasts/1.0",
    },
    custom_elements: [
      {
        "itunes:title": podcastData.title,
      },
      {
        "itunes:subtitle": podcastData.subtitle,
      },
      {
        "itunes:summary": podcastData.summary.substring(0, 3999),
      },
      {
        "itunes:type": podcastData.podcastType,
      },
      {
        "itunes:explicit": podcastData.explicit,
      },
      {
        "itunes:author": podcastData.authorName,
      },
      {
        "itunes:owner": [
          {
            "itunes:name": podcastData.ownerName,
          },
          {
            "itunes:email": podcastData.ownerEmail,
          },
        ],
      },
      {
        "itunes:image": {
          _attr: {
            href: podcastData.imageUrl,
          },
        },
      },
      {
        "itunes:category": [
          {
            _attr: {
              text: podcastData.category1,
            },
          },
          {
            "itunes:category": {
              _attr: {
                text: podcastData.subCategory1,
              },
            },
          },
        ],
      },
      {
        "itunes:category": [
          {
            _attr: {
              text: podcastData.category2,
            },
          },
          {
            "itunes:category": {
              _attr: {
                text: podcastData.subCategory2,
              },
            },
          },
        ],
      },
      {
        "itunes:category": [
          {
            _attr: {
              text: podcastData.category3,
            },
          },
          {
            "itunes:category": {
              _attr: {
                text: podcastData.subCategory3,
              },
            },
          },
        ],
      },
      {
        "googleplay:author": podcastData.authorName,
      },
      {
        "googleplay:description": podcastData.summary.substring(0, 999),
      },
      {
        "googleplay:explicit": podcastData.explicit,
      },
    ],
  };
};

// Get document, or throw exception on error
try {
  const podcastData = yaml.safeLoad(fs.readFileSync("podcast-rss-config.yml"));

  const fileList = fs
    .readdirSync(podcastData.markdownDir)
    .filter((name) => name.match(/\.md$/));

  if (!fileList.length) {
    console.error("No .md files found in ", podcastData.markdownDir);
    exit(1);
  }
  const episodes = [];
  for (const fileName of fileList) {
    const parsed = matter(
      fs.readFileSync(podcastData.markdownDir + "/" + fileName)
    );
    episodes.push(parsed);
  }

  episodes.sort(
    (ep1, ep2) =>
      new Date(ep2.data.publicationDate).getTime() -
      new Date(ep1.data.publicationDate).getTime()
  );

  const feedOptions = getFeedOptions(podcastData);
  const feed = new RSS(feedOptions);

  for (const episode of episodes) {
    const episodeData = episode.data;
    {
      feed.item({
        guid: episode.data.guid,
        title: episode.data.title,
        date: episode.data.publicationDate,
        description: marked(episode.content),
        url: podcastData.siteUrl + episode.data.slug,
        categories: episode.data.categories,
        author: episode.data.author,
        custom_elements: [
          {
            "itunes:title": episode.data.title,
          },
          {
            "itunes:subtitle":
              episode.data.subtitle !== ""
                ? episode.data.subtitle
                : episode.content.replace("\n", "").substring(0, 120) + "...",
          },
          episode.data.season && {
            "itunes:season": episode.data.season,
          },
          episode.data.episodeNumber && {
            "itunes:episode": episode.data.episodeNumber,
          },
          {
            "itunes:duration": episode.data.duration,
          },
          {
            "itunes:episodeDataType": episode.data.episodeDataType,
          },
          {
            "itunes:explicit": episode.data.explicit,
          },
          {
            "itunes:summary": episode.content.replace("\n", ""),
          },
          { "content:encoded": marked(episode.content) },
          {
            "itunes:author": episode.data.author,
          },
          {
            "itunes:image": {
              _attr: {
                href: episode.data.imgUrl,
              },
            },
          },
          {
            "googleplay:description": marked(episode.content),
          },
          {
            "googleplay:explicit": episode.data.explicit,
          },
        ],
        enclosure: {
          url: episode.data.audioUrl,
          size: episode.data.size,
          type: "audio/mpeg",
        },
      });
    }
  }
  fs.writeFileSync(podcastData.outputPath, feed.xml());
} catch (e) {
  console.log(e);
}
