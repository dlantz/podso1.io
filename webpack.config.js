const webpack = require("webpack");
const path = require("path");
const config = require("sapper/config/webpack.js");
const pkg = require("./package.json");
const sveltePreprocess = require("svelte-preprocess");

const mode = process.env.NODE_ENV;
const dev = mode === "development";

const alias = { svelte: path.resolve("node_modules", "svelte") };
const extensions = [".mjs", ".js", ".json", ".svelte", ".html", ".ts"];
const mainFields = ["svelte", "module", "browser", "main"];

module.exports = {
  client: {
    entry: config.client.entry(),
    output: config.client.output(),
    resolve: { alias, extensions, mainFields },
    module: {
      rules: [
        {
          test: /\.(svelte|html)$/,
          use: {
            loader: "svelte-loader",
            options: {
              preprocess: sveltePreprocess({}),
              dev,
              hydratable: true,
              hotReload: false, // pending https://github.com/sveltejs/svelte/issues/2377
            },
          },
        },
        {
          test: /\.svg$/,
          loader: "svg-inline-loader",
          options: {
            removeSVGTagAttrs: true,
          },
        },
        {
          test: /\.ts$/,
          loader: "ts-loader",
        },
        {
          test: /\.(sa|sc|c)ss$/,
          use: [
            "style-loader",
            "css-loader",
            {
              loader: "sass-loader",
              options: {
                sassOptions: {
                  includePaths: ["./src/theme", "./node_modules"],
                },
              },
            },
          ],
        },
        {
          test: /\.md$/,
          use: [{ loader: "gray-matter-loader" }],
        },
      ],
    },
    mode,
    plugins: [
      // pending https://github.com/sveltejs/svelte/issues/2377
      // dev && new webpack.HotModuleReplacementPlugin(),
      new webpack.DefinePlugin({
        "process.browser": true,
        "process.env.NODE_ENV": JSON.stringify(mode),
      }),
    ].filter(Boolean),
    devtool: dev && "inline-source-map",
  },

  server: {
    entry: config.server.entry(),
    output: config.server.output(),
    target: "node",
    resolve: { alias, extensions, mainFields },
    externals: Object.keys(pkg.dependencies).concat("encoding"),
    module: {
      rules: [
        {
          test: /\.(svelte|html)$/,
          use: {
            loader: "svelte-loader",
            options: {
              preprocess: sveltePreprocess({}),
              css: false,
              generate: "ssr",
              dev,
            },
          },
        },
        {
          test: /\.svg$/,
          loader: "svg-inline-loader",
          options: {
            removeSVGTagAttrs: true,
          },
        },
        {
          test: /\.ts$/,
          loader: "ts-loader",
        },
        {
          test: /\.(sa|sc|c)ss$/,
          use: [
            "css-loader",
            {
              loader: "sass-loader",
              options: {
                sassOptions: {
                  includePaths: ["./src/theme", "./node_modules"],
                },
              },
            },
          ],
        },
        {
          test: /\.md$/,
          use: [{ loader: "gray-matter-loader" }],
        },
      ],
    },
    mode: process.env.NODE_ENV,
    plugins: [
      // pending https://github.com/sveltejs/svelte/issues/2377
      // dev && new webpack.HotModuleReplacementPlugin(),
      new webpack.DefinePlugin({
        "process.browser": true,
        "process.env.NODE_ENV": JSON.stringify(mode),
      }),
    ].filter(Boolean),
    performance: {
      hints: false, // it doesn't matter if server.js is large
    },
  },

  serviceworker: {
    entry: config.serviceworker.entry(),
    output: config.serviceworker.output(),
    mode: process.env.NODE_ENV,
  },
};
